package org.dador;

import java.util.ArrayList;

/**
 *
 */
public class MultiTimePad {

    /**
     * Main function. Loads cryptogram and displays decryption
     *
     * @param args No arguments are processed
     */
    public static void main(final String[] args) {
        String index = "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E";

        String msg0 = "ce38a99f9c89fc89e8c5c14190f7fe3f0de5c388e3670420c57db02e66ee51";
        String msg1 = "d43fb89f9092ebdbeecad10494bbf6220deed5dce36d0620c270b23223d351";
        String msg2 = "d370add2df8ce29ae3c5dc0f87bbfe715ee9d3daf27c546ddf6ba0356cf451";
        String msg3 = "d235ecd68cdcfa93e88bda0f8ce2bf2148fec3c7f928006f966ca12970ee51";
        String msg4 = "cd38a9d1df8fe694f8c7d14197febf3c48e9c488e3675464d938a7346ae940";
        String msg5 = "d370b8d79692e5dbf9c3d018c0e8f73e58e0d488f167186cd96ff3346af751";
        String msg6 = "ce38a5ccdf95fddbfddec70492bbeb394ce290dcff690020d976b67c6ae951";
        String[] messages = new String[]{msg0, msg1, msg2, msg3, msg4, msg5, msg6};
        byte[] key;
        byte[][] byteArrayMsg;
        int nbMsg = messages.length;
        byte[] tmpByteMsg;
        int i;
        byteArrayMsg = new byte[nbMsg][];

        String displayIndex = HexConverters.toPrintableHexFromByteArray(HexConverters.toByteArrayFromHex(index));
        System.out.println("Original Cryptograms :");
        System.out.println(displayIndex);
        // Transforme les messages sous un format "tableau d'octets" pour pouvoir les manipuler
        for (i = 0; i < nbMsg; i++) {
            tmpByteMsg = HexConverters.toByteArrayFromHex(messages[i]);
            byteArrayMsg[i] = tmpByteMsg;
            System.out.println(HexConverters.toPrintableHexFromByteArray(byteArrayMsg[i]));
        }

        System.out.println();

        key = new byte[msg1.length() / 2];
        // Fill in the key ...
        //colonne 11 : " "
      
        key[0x00]=(byte)(0x49^0xd3);
		key[0x01]=(byte)(0x70^0x20);
		key[0x02]=(byte)(0xec^0x20);
        key[0x03]=(byte)(0x9f^0x20);
        key[0x04]=(byte)(0xdf^0x20);
        key[0x05]=(byte)(0xdc^0x20);
        key[0x06]=(byte)(0x73^0xfd);
        key[0x07]=(byte)(0xdb^0x20);
        key[0x08]=(byte)(0xe8^0x65);
        key[0x09]=(byte)(0x8b^0x20);
        key[0x0a]=(byte)(0x74^0xc1);
        key[0x0b]=(byte)(0x41^0x20);
        key[0x0c]=(byte)(0xc0^0x20);
        key[0x0d]=(byte)(0xbb^0x20);
        key[0x0e]=(byte)(0xbf^0x20);
        key[0x0f]=(byte)(0x71^0x20);
        key[0x10]=(byte)(0x0d^0x20);
        key[0x11]=(byte)(0x69^0xe5);
        key[0x12]=(byte)(0x90^0x20);
		key[0x13]=(byte)(0xa8);
		key[0x14]=(byte)(0x74^0xe3);
		key[0x15]=(byte)(0x28^0x20);
		key[0x16]=(byte)(0x54^0x20);
		key[0x17]=(byte)(0x20^0x20);
		key[0x18]=(byte)(0x96^0x20);
		key[0x19]=(byte)(0x38^0x20);
		key[0x1a]=(byte)(0xf3^0x20);
		key[0x1b]=(byte)(0x7c^0x20);
		key[0x1c]=(byte)(0x23^0x20);
		key[0x1d]=(byte)(0x74^0xee);
		key[0x1e]=(byte)(0x2e^0x51);
        System.out.println("Key :");
        System.out.println(displayIndex);
        System.out.println(HexConverters.toPrintableHexFromByteArray(key));

        // Affichage des messages décodés
        System.out.println();
        System.out.println("Decoded messages :");
        for (i = 0; i < nbMsg; i++) {
            tmpByteMsg = HexConverters.xorArray(key, byteArrayMsg[i]);
            System.out.println(HexConverters.toPrintableString(tmpByteMsg));
        }
        
        System.out.println("\nProduit C0 XOR Cn");
   
        
   
       
       ArrayList<byte[]> P = new ArrayList<>();
       
       for (int i1 = 0; i1 < 7; i1++) {
    	   P.removeAll(P);
    	   for (int j = 0; j < 7; j++) 
           {
        	   P.add(HexConverters.xorArray(byteArrayMsg[i1],byteArrayMsg[j]));
           }
    	   System.out.println("\nProduit C"+i1+" XOR Cn");
    	   System.out.println(displayIndex);
    	   P.forEach(s -> System.out.println(HexConverters.toPrintableHexFromByteArray(s)));
    	   System.out.println("\n");
		
       }
       
       
      
      
       
       byte[] cle = HexConverters.xorArray(HexConverters.toByteArrayFromHex("20"),HexConverters.toByteArrayFromHex("9f"));
       
       
    }
    
  
}